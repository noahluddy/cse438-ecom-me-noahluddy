package com.example.cse438.studio1.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.cse438.studio1.R
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.nav_header_main.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String) : Fragment() {

    private var parentContext: Context = context
    private var query = query

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        DONE: implement this function to inflate the fragment_result_list.xml file for the container;
//        Log.d("Android:", "Incomplete")
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val newStr: String = query_text.text.toString() + query
        query_text.text = newStr
    }

}